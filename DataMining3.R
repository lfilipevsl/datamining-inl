# Data mining chpt3

# LOAD DATA
# Read CSV
states <- read.csv("D:\\MEGA\\fil\\inLearning\\DataMining\\ClusterData.csv", header = T)
colnames(states)

#Save numerical data only
st <- states[, 3:27]
st
row.names(st) <- states[, 2]
colnames(st)
row.names(st)

# Sports search data only
sports <- st[, 8:11]
head(sports, 9)

# CLUSTERING ###############################################

# Create distance matrix
d <- dist(st)

# Hierarchical clustering
c <- hclust(d)
c # Info on clustering

# Plot dendrogram of clusters
plot(c, main = "Cluster with All Searches and Personality")

# Or nest commands in one line (for sports data)
plot(hclust(dist(sports)), main = "Sports Searches")

# CLEAN UP #################################################

# Clear workspace
rm(list = ls()) 

# Clear plots
dev.off()

# Clear console
cat("\014")  # ctrl+L
