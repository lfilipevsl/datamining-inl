Repository with files developed following the course "Data Science Foundations: Data Mining" by Barton Poulson at Linkedin: https://www.linkedin.com/learning/data-science-foundations-data-mining 

This contains mostly the datsets used as well as R scripts. The datasets are the files made available by the author. The scripts are the ones I have done followind or adapting the resources he has made available in the course.

I am also using this as a practice on using GIT and GitLab in this case.